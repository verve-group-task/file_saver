package processor

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"filesaver/storage"
	"filesaver/types"
)

type Processor interface {
	Process() error
}

type processor struct {
	storage  storage.DataBase
	filepath string
}

func (p *processor) Process() error {
	// Open the file
	csvFile, err := os.Open(p.filepath)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvFile)
	//r := csv.NewReader(bufio.NewReader(csvFile))

	// Iterate through the records
	i := 1
	for {
		// Read each record from csv
		row, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		price, err := strconv.ParseFloat(row[1], 64)
		if err != nil {
			return err
		}

		expirationDate, err := time.Parse("2006-01-02 15:04:05 -0700 MST", row[2])
		if err != nil {
			return err
		}

		// Create a record
		record := types.Promotion{
			ID:             row[0],
			Price:          price,
			ExpirationDate: expirationDate,
		}

		// Save the record
		if err = p.storage.Set(i, &record); err != nil {
			return err
		}
		i++
	}

	return nil
}

func NewProcessor(storage storage.DataBase, filepath string) Processor {
	return &processor{
		storage:  storage,
		filepath: filepath,
	}
}
