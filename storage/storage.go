package storage

import "filesaver/types"

type DataBase interface {
	Get(key string) (*types.Promotion, error)
	Set(id int, value *types.Promotion) error
	Close() error
	Flusher
}

type Flusher interface {
	Flush() error
}
