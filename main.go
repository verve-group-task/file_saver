package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"github.com/joho/godotenv"

	"filesaver/processor"
	"filesaver/storage"
)

func main() {

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	if err := godotenv.Load(); err != nil {
		log.Fatal().Msg(err.Error())
	}
	//redisDB, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	//if err != nil {
	//	log.Fatal().Msg(err.Error())
	//}
	//rdb := storage.NewRedisClient(os.Getenv("REDIS_ADDRESS"), os.Getenv("REDIS_PASSWORD"), redisDB)
	psql := storage.NewPostgres(fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME")))
	proc := processor.NewProcessor(psql, os.Getenv("FILE_PATH"))
	level, err := zerolog.ParseLevel(os.Getenv("LEVEL"))
	if err != nil {
		log.Err(err).Msg("Failed to parse level, setting to debug level")
		level = zerolog.DebugLevel
	}

	log.Level(level)

	log.Info().Msg("Starting")
	ticker := time.NewTicker(1 * time.Second)
	lock := new(sync.Mutex)
	for {
		select {
		case <-ticker.C:
			log.Info().Msg("ticking")
			go func(lock *sync.Mutex) {
				lock.Lock()
				defer lock.Unlock()
				if err = psql.Flush(); err != nil {
					log.Fatal().Msg(err.Error())
				}
				log.Info().Msg("starting thread")
				if err = proc.Process(); err != nil {
					log.Err(err).Msg("Failed to process file")
					return
				}
				os.Exit(1)
			}(lock)
		case <-sigc:
			log.Info().Msg("Shutting down")
			ticker.Stop()
			close(sigc)
			os.Exit(0)
		}
	}
}
